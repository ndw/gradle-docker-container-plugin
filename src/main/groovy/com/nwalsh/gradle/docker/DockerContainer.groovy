package com.nwalsh.gradle.docker

import org.gradle.api.Project
import org.gradle.api.GradleException

/**
 * The <code>DockerContainer</code> encapsulates interaction with the
 * <code>docker</code> and <code>docker-compose</code> commands for
 * use in Gradle build scripts.
 *
 * <p>I manage environments for different projects in different
 * collections of Docker containers.</p>
 *
 * <p>I find it convenient to start and stop these containers from
 * Gradle. In addition, tasks that require the containers should be
 * able to check that they are running.</p>
 *
 * <p>This Gradle plugin simple encapsulates those interactions. It
 * doesn’t do anything very sophisticated with Docker or any of the
 * Docker APIs.</p>
 */

public class DockerContainer {
  private static Project project = null;
  private static DockerContainerPluginExtension extension = null;

  private DockerContainer() {
    // This class should never be instantiated.
  }

  /**
   * Where is <code>docker</code>?
   * 
   * @return The absolute path of the <code>docker</code> executable,
   * or <code>null</code> if it could not be found on the user's <code>PATH</code>.
   */
  public static String docker() {
    return extension.getDocker()
  }

  /**
   * Where is <code>docker-compose</code>?
   * 
   * @return The absolute path of the <code>docker-compose</code> executable
   * or the word "<code>docker</code>" if the compose command is a subcommand of docker.
   * Returns <code>null</code> if it could not be found on the user's <code>PATH</code>.
   */
  public static String dockerCompose() {
    return extension.getCompose()
  }

  /**
   * Test if a container is running.
   *
   * <p>This method takes the <em>name</em> of a container and checks to see
   * if that container is running. Most underlying <code>docker</code> commands use
   * the container ID, but those are essentially random. The name can be set in the
   * <code>docker-compose.yaml</code> file to something that is stable.</p>
   *
   * @param name The <em>name</em> of a container.
   * @return True if a container with that name is currently running.
   */
  public static boolean running(String name) {
    return containers().containsKey(name)
  }

  /**
   * Test if all of a list of containers are running.
   *
   * <p>This method takes a list of the <em>name</em>s of containers
   * and checks to see if they are all running. Most underlying
   * <code>docker</code> commands use the container ID, but those are
   * essentially random. The name can be set in the
   * <code>docker-compose.yaml</code> file to something that is
   * stable.</p>
   *
   * @param names The list of <em>names</em> of containers.
   * @return True if all the named containers are running
   */
  public static boolean allRunning(ArrayList<String> names) {
    boolean allrunning = true;
    names.each { name ->
      allrunning = allrunning && running(name)
    }
    return allrunning
  }

  /**
   * Test if any of a list of containers are running.
   *
   * <p>This method takes a list of the <em>name</em>s of containers
   * and checks to see if any of them are running. Most
   * underlying <code>docker</code> commands use the container ID, but
   * those are essentially random. The name can be set in the
   * <code>docker-compose.yaml</code> file to something that is
   * stable.</p>
   *
   * @param names The list of <em>names</em> of containers.
   * @return True if none of the named containers are running
   */
  public static boolean anyRunning(ArrayList<String> names) {
    boolean anyrunning = false;
    names.each { name ->
      anyrunning = anyrunning || running(name)
    }
    return anyrunning
  }

  /**
   * Assert that <code>docker</code> is available.
   *
   * <p>This method raises an exception if <code>docker</code> could not
   * be found on the user's path. It's just a convenience to avoid having
   * to write the test and raise the exception yourself in the build script.
   *
   * @throws GradleException if Docker is unavailable.
   */
  public static void assertDockerAvailable() {
    if (extension.getDocker() == null) {
      throw new GradleException("Docker is unavailable (not found on PATH)")
    }
  }

  /**
   * Assert that <code>docker-compose</code> is available.
   *
   * <p>This method raises an exception if <code>docker-compose</code> could not
   * be found on the user's path. It's just a convenience to avoid having
   * to write the test and raise the exception yourself in the build script.
   *
   * @throws GradleException if Docker Compose is unavailable.
   */
  public static void assertDockerComposeAvailable() {
    if (extension.getCompose() == null) {
      throw new GradleException("Docker compose is unavailable (not found on PATH)")
    }
  }

  /**
   * Assert that the container named <code>name</code> running.
   *
   * <p>This method raises an exception if the named container is not running.
   * It's just a convenience to avoid having
   * to write the test and raise the exception yourself in the build script.
   *
   * @throws GradleException if the container is not running.
   */
  public static void assertRunning(String name) {
    assertStatus([name], true)
  }

  /**
   * Assert that the named containers are (all) running.
   *
   * <p>This method raises an exception if any of the named containers is not running.
   * It's just a convenience to avoid having
   * to write the test and raise the exception yourself in the build script.
   *
   * @throws GradleException if the container is not running.
   */
  public static void assertRunning(ArrayList<String> names) {
    assertStatus(names, true)
  }

  /**
   * Assert that the container named <code>name</code> <em>is not</em> running.
   *
   * <p>This method raises an exception if the named container is running.
   * It's just a convenience to avoid having
   * to write the test and raise the exception yourself in the build script.
   *
   * @throws GradleException if the container is running.
   */
  public static void assertNotRunning(String name) {
    assertStatus([name], false)
  }

  /**
   * Assert that <em>none</em> of the named container are running.
   *
   * <p>This method raises an exception if any of the named containers are running.
   * It's just a convenience to avoid having
   * to write the test and raise the exception yourself in the build script.
   *
   * @throws GradleException if the container is running.
   */
  public static void assertNotRunning(ArrayList<String> names) {
    assertStatus(names, false)
  }

  private static void assertStatus(ArrayList<String> names, Boolean status) {
    names.each { name ->
      if (running(name) != status) {
        String mstat = status ? "is not" : "is"
        throw new GradleException("Docker container ${mstat} running: ${name}")
      }
    }
  }

  /**
   * Get the running containers and their IDs.
   *
   * <p>This method returns a map where the keys are all the names of
   * running containers. The value associated with each key is the ID
   * of the corresponding container.</p>
   *
   * @return A map of container name/ID pairs.
   */
  public static HashMap<String,String> containers() {
    HashMap<String,String> cmap = new HashMap<String,String> ()

    if (extension.getDocker() == null) {
      return cmap
    }

    ProcessBuilder pb = new ProcessBuilder()
      .command(extension.getDocker(),
               "ps", "-a", "--format={{.ID}} {{.Names}} {{.Status}}")
    Process proc = pb.start();

    BufferedReader reader = new BufferedReader(
      new InputStreamReader(proc.getInputStream()))

    String line = reader.readLine()
    while (line != null) {
      def parts = line.split(" ")
      if (parts.length > 2) {
        def id = parts[0]
        def name = parts[1]
        def status = parts[2]

        if (name.contains(":")) {
          name = name.split(":")[0];
        }
      
        if (!status.startsWith("Exited")) {
          cmap.put(name, id)
        }
      }
      line = reader.readLine()
    }

    proc.waitFor()
    reader.close()
    return cmap
  }

  /**
   * Find the ID of a running container.
   *
   * <p>This method takes the <em>name</em> of a container and checks
   * to see if that container is running. If it is, it returns the ID
   * of the container. In most cases, this ID will be needed to
   * interact with the container using the <code>docker</code> command.</p>
   *
   * @param name The <em>name</em> of a container.
   * @return The ID of the container, or null if it isn't running
   */
  public static String id(String name) {
    if (containers().containsKey(name)) {
      return containers().get(name)
    } else {
      return null
    }
  }

  /** Evaluate a docker command.
   *
   * @param cl The closure containing the environment
   */
  public static void docker(Closure cl) {
    assertDockerAvailable()

    DockerContainerCommandConfiguration config = new DockerContainerCommandConfiguration();
    cl.delegate = config
    cl.call()
    if (config.command == null) {
      throw new GradleException("No compose command specified")
    }

    ArrayList<String> args = new ArrayList()
    args.add(extension.getDocker())

    if (config.options != null) {
      args.addAll(config.options)
    }

    args.add(config.command)

    if (config.arguments != null) {
      args.addAll(config.arguments)
    }

    run(config, args)
  }

  /** Evaluate a docker compose command.
   *
   * @param cl The closure containing the environment
   */
  public static void compose(Closure cl) {
    assertDockerAvailable()
    assertDockerComposeAvailable()

    DockerContainerCommandConfiguration config
      = new DockerContainerCommandConfiguration();

    cl.delegate = config
    cl.call()
    if (config.command == null) {
      throw new GradleException("No compose command specified")
    }

    String exec = extension.getCompose()
    ArrayList<String> args = new ArrayList()

    if (exec == "docker") {
      exec = extension.getDocker()
      args.add(exec)
      args.add("compose")
    } else {
      args.add(exec)
    }

    if (config.options != null) {
      args.addAll(config.options)
    }

    args.add(config.command)

    if (config.arguments != null) {
      args.addAll(config.arguments)
    }

    run(config, args)
  }

  private static run(DockerContainerCommandConfiguration config, ArrayList<String> args) {
    ProcessBuilder pb = new ProcessBuilder();

    String workingDir = extension.workingDir
    if (config.workingDir != null) {
      workingDir = config.workingDir
    }

    if (workingDir != null) {
      pb = pb.directory(new File(workingDir))
    }

    pb = pb.redirectErrorStream(true)

    boolean debug = extension.getDebug()
    if (config.debug != null) {
      debug = config.debug
    }
    if (debug) {
      println("DockerContainer: ${args.join(' ')}")
    }

    pb = pb.command(args)
    Process proc = pb.start()

    ArrayList<String> lines = new ArrayList<>()
    boolean verbose = false

    if (config.command in extension.verboseCommands) {
      verbose = true
    } else {
      verbose = extension.getVerbose()
    }

    if (config.verbose != null) {
      verbose = config.verbose
    }

    BufferedReader stdout = new BufferedReader(new InputStreamReader(proc.getInputStream()))
    String line = stdout.readLine()
    while (line != null) {
      if (verbose) {
        println(line)
      } else {
        lines.add(line)
      }
      line = stdout.readLine()
    }

    proc.waitFor()
    stdout.close()

    if (proc.exitValue() != 0) {
      for (String eline : lines) {
        println(eline)
      }
      throw new GradleException("DockerContainer failed")
    }
  }

  protected static void setProject(Project proj) {
    project = proj
    extension = project.extensions['docker_container']
  }
}
