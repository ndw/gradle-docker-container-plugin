package com.nwalsh.gradle.docker

import org.gradle.api.Project
import org.gradle.api.Plugin

class DockerContainerPlugin implements Plugin<Project> {
  @Override
  void apply(Project project) {
    project.extensions.create("docker_container", DockerContainerPluginExtension)
    DockerContainer.setProject(project)
  }
}
