package com.nwalsh.gradle.docker

class DockerContainerCommandConfiguration {
  private String workingDir = null
  private String command = null
  private String[] options = null
  private String[] arguments = null
  private Boolean debug = null
  private Boolean verbose = null

  public void setOptions(String option) {
    options = new String[] {option}
  }

  public void setOptions(List opts) {
    options = new String[opts.size()]
    //opts.toArray(options)
    int pos = 0;
    for (String opt : opts) {
      options[pos] = opt;
      pos++;
    }
  }

  public void setArgs(String arg) {
    arguments = new String[] {arg}
  }

  public void setArgs(List opts) {
    arguments = new String[opts.size()]
    // After working for weeks, maybe months, this suddenly
    // started raising ArrayStoreException:
    //   opts.toArray(arguments)
    // I have no explanation for why, but I'm avoiding it.
    int pos = 0;
    for (String opt : opts) {
      arguments[pos] = opt;
      pos++;
    }
  }
}
