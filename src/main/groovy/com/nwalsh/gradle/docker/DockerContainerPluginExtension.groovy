package com.nwalsh.gradle.docker

import org.gradle.internal.os.OperatingSystem;

class DockerContainerPluginExtension {
  private static final String pathSep = System.getProperty("path.separator")
  private static final String fileSep = System.getProperty("file.separator")
  private static final String dockerFn = "docker"
  private static final String dockerComposeFn = "docker-compose"
  private String workingDir = null
  private String docker = null
  private String compose = null
  private boolean verbose = false
  private boolean debug = false
  private boolean searched = false
  private String[] verboseCommands = ["ps", "logs", "exec"]

  public void configure(Closure cl) {
    cl.delegate = this
    cl()
  }

  // Delegate getters and setters

  public void setWorkingDir(String dir) {
    workingDir = dir
  }

  public String getWorkingDir() {
    return workingDir
  }

  public void setDocker(String cmd) {
    docker = cmd
  }

  public String getDocker() {
    if (!searched) {
      searchPath()
    }
    return docker
  }

  public void setCompose(String cmd) {
    compose = cmd
  }

  public String getCompose() {
    if (!searched) {
      searchPath()
    }
    return compose
  }

  public void setVerbose(boolean value) {
    verbose = value
  }

  public boolean getVerbose() {
    return verbose
  }

  public void setDebug(boolean value) {
    debug = value
  }

  public boolean getDebug() {
    return debug
  }

  public void setVerboseCommands(String command) {
    verboseCommands = new String[] { command }
  }

  public void setVerboseCommands(List<String> commands) {
    verboseCommands = new String[commands.size()]
    commands.toArray(verboseCommands)
  }

  public String[] getVerboseCommands() {
    return verboseCommands
  }

  // ============================================================

  private void searchPath() {
    searched = true
    String dockerExec = null
    String composeExec = null

    String searchDocker = dockerFn
    String searchCompose = dockerComposeFn
    if (OperatingSystem.current().isWindows()) {
      searchDocker += ".exe"
      searchCompose += ".exe"
    }

    System.getenv("PATH").split(pathSep).each { dir ->
      if (dockerExec == null) {
        File fn = new File("${dir}${fileSep}${searchDocker}")
        if (fn.exists() && fn.canExecute()) {
          dockerExec = fn.getAbsolutePath()
        }
      }

      if (composeExec == null) {
        File fn = new File("${dir}${fileSep}${searchCompose}")
        if (fn.exists() && fn.canExecute()) {
          composeExec = fn.getAbsolutePath()
        }

        // What if 'compose' is a feature of the 'docker' command?
        if (dockerExec != null) {
          ProcessBuilder pb = new ProcessBuilder(dockerExec, "help", "compose")
          Process p = pb.start();
          BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))
          String line = reader.readLine()
          while (line != null) {
            if (line.contains("Usage:") && line.contains("docker compose [OPTIONS]")) {
              composeExec = "docker"
            }
            line = reader.readLine()
          }
        }
      }
    }

    if (docker == null) {
      docker = dockerExec
    }
    if (compose == null) {
      compose = composeExec
    }
  }
}
