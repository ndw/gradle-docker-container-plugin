# Gradle Docker Container Plugin

This plugin encapsulates interacting with the `docker` and
`docker-compose` commands to manage containers from a Gradle build
script.

I manage environments for different projects in different collections
of Docker containers:

* I have Node.js, PostgreSQL, and Nginx containers for web projects.
* For the [XProc 3.0 test suite](https://github.com/xproc/3.0-test-suite]),
  there’s a container that runs a web server for testing the `p:http-request`
  step.
* In another project, I have a container that just runs an SQL server for testing.

I find it convenient to start and stop these containers from Gradle.
In addition, tasks that require the containers should be able to check
that they are running.

I used Gradle `exec` and various inline-Groovy code for a while, but
decided to push it all off into a plugin for ease-of-use. This Gradle
plugin simple encapsulates those interactions. It doesn’t do anything
very sophisticated with Docker or any of the Docker APIs.

It consists of a single class with some static methods. Behind the
scenes, it searches the user’s PATH to find the `docker` and
`docker-compose` commands.

## Configuration

You can configure a few things in a `docker_container.configure` closure,
for example:

```groovy
docker_container.configure {
  workingDir = "docker"
}
```

Values set in the configuration closure apply globally, but you can override
the global settings on each command.

* `workingDir` tells the plugin where to run the docker compose commands.
  (This lets you keep the `docker-compose.yml` file and other Docker related
  files in a separate directory.)
* `docker` tells the plugin where to find the Docker executable. If
  you don’t specify this, the plugin will search your `$PATH`
  for `docker` and `docker.exe`.
* `compose` tells the plugin where to find the Docker compose executable. Like
  the `docker` setting, the plugin will search your path. The plugin will also
  test to see if `compose` is a subcommand of your `docker` executable and will
  use that if it can. This is indicated by making the `compose` setting the
  string `docker`.
* `debug` can be true or false. If `debug` is true, the plugin will print
  the Docker and Docker compose commands that it runs before running them.
* `verbose` can be true or false. If `verbose` is true, the plugin will print
  the output from Docker and Docker compose. Otherwise, it just swallows
  that output. The output isn’t especially pretty in this context because
  the Docker commands don’t have access to an interactive, color TTY.
* `verboseCommands` is a list of commands that should be `verbose` by default.
  Some commands, `ps` and `logs` for example, don’t make any sense if the
  verbose flag is false.  Rather than make you remember to specify
  `verbose=true` for each of them, `verboseCommands` lets you list them.
  By default, `ps` and `logs` are verbose, uh, by default.

## API

The basic API is quite small.

### docker and compose

`DockerContainer.docker(closure)` runs the Docker command described by
the closure, `DockerContainer.compose(closure)` runs the Docker
compose command described by the closure.

Each of these closures support three commands beyond the the
configuration items described above: `command`, `args`, and `options`.
For example:

```groovy
DockerContainer.compose {
  options = ["--env-file", "stage.env"]
  command = "run"
  args = ["-v", "myvolume", "myservice"]
}
```

Options come before the command and arguments come after.

The same is true for the `docker` command:

```groovy
DockerContainer.docker( {
  debug = true
  command = "ps"
  args = ["-a", "--format={{.ID}} {{.Names}} {{.Status}}"]
})
```

### Is it running?

Sometimes it’s useful to take action depending on whether or not a
container is running.

* `DockerContainer.running(name)` returns true if a container *named*
  `name` is running. Docker containers have names and IDs. The IDs are
  essentially random, so this API assumes you know the names of the
  containers that you’ve started.
* `DockerContainer.allRunning(names)` returns true if all of the
  named containers in the list `names` are running.
* `DockerContainer.anyRunning(names)` returns true if any of the
  named containers in the list `names` is running.

For example:

```groovy
doLast {
  if (!DockerContainer.allRunning(["postgis_so", "nginx_so", "nodejs_so"]) {
    DockerContainer.compose {
      command = "up"
      args = ["-d"]
    }
  }
}
```

### Assertions

When I first started working on these ideas, I used to have “started”
and “stopped” tasks that would fail if the Docker containers I needed
were not (or were) running.

These days, I tend to simply start or stop the containers automatically
rather than fail. But these APIs still exist:

* `DockerContainer.assertDockerAvailable()` raises an exception of the
  `docker` command wasn’t found on the users’s `PATH`.
* `DockerContainer.assertDockerComposeAvailable()` raises an exception
  of the `docker-compose` command wasn’t found.
* `DockerContainer.assertRunning(name)` raises an exception if a
  container *named* `name` is not already running. (Also supports a list of names.)
* `DockerContainer.assertNotRunning(name)` raises an exception if a
  container *named* `name` *is* already running. (Also supports a list of names.)

### Getting container ids

Docker commands expect (perhaps not unreasonably) that you’ll identify the container
with its ID. Unfortunately, those IDs are essentially random. But, fortunately, you
can give containers names when you start them. You can get the name/ID mapping for
all containers or any specific container.

* `DockerContainer.containers()` returns a map of the currently
  running containers. The keys are the container *names* and the
  values are the container IDs.
* `DockerContainer.id(name)` returns the ID of the container named
  `name`. Returns `null` if no suitably named container is found.

### Running what?

Finally, you can ask where the plugin found the Docker executables.

* `DockerContainer.docker()` returns the absolute path of the `docker`
  command, or `null` if it can’t be found on the user’s `PATH`.
* `DockerContainer.dockerCompose()` returns the absolute path of the
  `docker-compose` command, just the string `docker` if the plugin
  believes it can use the `compose` subcommand of `docker`, or `null`
  if it failed to find compose.

## Example

The `example` directory contains a Gradle build script with tasks to
start, stop, and interact with a container. 


